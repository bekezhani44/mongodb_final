<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Genre;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class AlbumController extends Controller
{
    public function index()
    {
        $albums = Album::all();
        $genres = Genre::all();
        $users = User::all();
        return view('album.index', compact(['albums', 'genres', 'users']));
    }

    public function create()
    {   
        $genres = Genre::all();
        $tags = Tag::all();
        $users = User::all();
        return view('album.create', compact(['genres', 'tags', 'users']));
    }

    public function show($id)
    {
        $album = Album::find($id);
        $user = User::find($album->user_id);
        return view('album.show', compact(['album', 'user']));
    }

    public function listen($id)
    {
        $album = Album::find($id);
        $albumName = explode(' ', trim($album->name));
        // foreach ($album->songs as $song) {
        //     $file = new mp3Controller("storage/albums/".$albumName[0]."/".$song->url);
        //     $duration = $file->getDuration();
        //     $length = mp3Controller::formatTime($duration);
        //     $song['length'] = $length;
        // }
        return view('album.listen', compact(['album', 'albumName']));
    }

    public function album_genre(Genre $genre)
    {
        $albums = Album::where('genre_id', $genre->id)->get();
        $genres = Genre::all();
        $users = User::all();
        return view('album.index', compact(['albums', 'genres', 'users']));
    }

    public function album_like($id)
    {
        $album = Album::find($id);
        $user = Auth::user();
        $user->albums()->attach($album);
        $album->users()->attach($user);

        return redirect()->route('album.show', $id);
    }

    public function album_delete($id)
    {
        $user = Auth::user();
        $album = Album::find($id);
        $album->users()->detach($user->id);
        $user->albums()->detach($album->id);
        return redirect()->route('album.show', $id);
    }

    public function store(Request $request)
    {   
        $album = new Album();
        $album->name = $request->input('name');
        $album->user_id = $request->input('artist');
        $album->year = $request->input('year');
        $album->cover = $request->cover->store('uploads', 'public');
        $image = Image::make(public_path("storage/{$album->cover}"))->fit(265, 265);
        $image->save();
        $album->genre_id = $request->input('genre');

        $album->save();
        
        // $tag = Tag::raw()->aggregate([
        //     [
        //         '$match' => ['name'=> $request->input('tag')]
        //     ],
        //     [
        //         '$project' => ['_id' => 1]
        //     ]
        // ]);
        // foreach ($tag as $document) {
        //     $tag_id = $document->_id;
        // }
        
        $tag = Tag::find($request->input('tag'));
        $album->tags()->attach($tag);
        $tag->albums()->attach($album);
        

        return redirect()->route('album.index')->with('success', ' Album was added');
    }

    public function edit($id)
    {
        $album = Album::find($id);
        $gen = Genre::all();
        $tags = Tag::all();
        $users = User::all();
        return view('album.edit', compact(['album', 'gen', 'tags', 'users']));
    }

    public function update(Request $request, $id)
    {   
        $album = Album::find($id);
        $album->update($request->only('name', 'user_id', 'year', 'genre_id'));
        $album->tags()->attach($request->only('tag'));
        return redirect()->route('album.index')->with('success', ' Album was updated successfully with ID: '.$album->id);
    }

    public function destroy($id)
    {
        $tag = new Tag();
        $album = Album::find($id);
        $album->tags()->detach($tag->id);
        $album->delete();

        return redirect()->route('album.index')->with('success', ' Album was deleted successfully with ID: '.$album->id);
    }
}
