<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
  
    public function signin()
    {
        return view('auth.signin');
    }

    public function post_signin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:255',
            'password' => 'required|min:6'
        ]);

        if(!Auth::attempt($request->only('email', 'password'), $request->has('remember')))
        {
            return redirect()->back()->with('danger', 'Login or password is incorrect');
        }

        return redirect()->route('profile.index',  Auth::user()->username)->with('success', 'You are logged in!');
    }

    public function signup()
    {
        return view('auth.signup');
    }

    public function post_signup(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|email|max:255',
            'username' => 'required|unique:users|alpha_dash|max:20',
            'password' => 'required|min:6'
        ]);

        $user = User::create([
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
        ]);

        $role = Role::all()->first();

        $user->roles()->attach($role);
        $role->users()->attach($user);
        
        return redirect()->route('home.index')->with('success', 'You are registered!');
    }

    public function log_out()
    {
        Auth::logout();

        return redirect()->route('home.index');
    }

}
