<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function index()
    {
        $genres = Genre::all();
        return view('genre.index', compact('genres'));
    }

    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
        $genre = new Genre();
        $genre->name = $request->input('name');
    
        $genre->save();

        return redirect()->route('genre.index')->with('success', 'Genre was added');
    }

    public function edit($id)
    {
        $genre = Genre::find($id);
        return view('genre.edit', compact('genre'));
    }

    public function update(Request $request, $id)
    {
        $genre = Genre::find($id);
        $genre->update($request->only('name'));
        return redirect()->route('genre.index')->with('success', ' Album was updated successfully with ID: '.$genre->id);
    }

    public function destroy($id)
    {
        $genre = Genre::find($id);
        $genre->delete();

        return redirect()->route('genre.index')->with('success', ' Genre was deleted successfully with ID: '.$genre->id);
    }
}
