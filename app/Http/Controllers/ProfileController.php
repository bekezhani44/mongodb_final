<?php

namespace App\Http\Controllers;

use App\Models\Album;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Song;
use App\Models\Genre;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;

use function PHPSTORM_META\type;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {
        $users = User::all();
        $user = User::where('username', $username)->first();
        $role = Auth::user()->roles()->get()->first();
        if(!$user) {
            return abort(404);
        }

        if(request()->category) {
            if(request()->category == 'albums') {
                $albums = $user->albums()->get();
                if($user->hasAnyRole('artist'))
                {
                    // $followersCount = $user->artist->followers->count();
                    return view('profile.index', compact(['user', 'role', 'albums', 'users']));
                }
        
                return view('profile.index', compact(['user', 'role', 'albums', 'users']));
            }

            else {
                $artist_albums = Album::where('user_id', $user->id)->get();
                if($user->hasAnyRole('artist'))
                {
                    // $followersCount = $user->artist->followers->count();
                    return view('profile.index', compact(['user', 'role', 'artist_albums']));
                }
        
                return view('profile.index', compact(['user', 'sub', 'plans', 'role', 'artist_albums']));
            }
        }

        return view('profile.index', compact(['user', 'role']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('_id', $id)->first();
        return view('profile.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->only('username', 'email', 'first_name', 'last_name'));
        return redirect()->route('profile.index', $user->username);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Artist

    public function station($username)
    {
        return view('artist.station');
    }

    public function artists()
    {
        $users = User::all();
        return view('artist.index', compact('users'));
    }

    public function album_create()
    {
        $genres = Genre::all();
        $tags = Tag::all();
        return view('artist.album_create', compact(['genres', 'tags']));
    }

    public function album_edit($username, $id)
    {
        $album = Album::find($id);
        $gen = Genre::all();
        $tags = Tag::all();
        $user = User::where('username', $username)->first();

        return view('artist.album_edit', compact(['album', 'gen', 'tags', 'user']));
    }

    public function songs($username)
    {
        $user = User::where('username', $username)->first();
        $album_id = $user->albums()->get()->pluck('id');

        if(Auth::user()->username != $username) {
            return redirect()->back();
        }

        $songs = Song::all();

        return view('artist.songs', compact(['songs', 'album_id']));
    }

    public function song_create($username)
    {
        $user = User::where('username', $username)->first();
        $albums = Album::where('user_id', $user->id)->get();

        if(sizeof($albums) == 0) {
            return redirect()->back()->with('success', "You don't have any album");
        }

        return view('artist.song_create', compact(['user', 'albums']));
    }
}
