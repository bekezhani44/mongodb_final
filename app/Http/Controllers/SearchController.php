<?php

namespace App\Http\Controllers;

use App\Models\Album;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
        $query = request('query');
        if(!$query) {
            return redirect()->back();
        }

        $albums = Album::all();

        $result = $albums->filter(function ($album) {
            $query = request('query');
            if(str_contains(strtolower($album), strtolower($query))) {
                return $album;
            }
        });

        return view('search.index', compact('result'));
    }
}
