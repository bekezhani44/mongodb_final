<?php

namespace App\Http\Controllers;

use App\Models\Song;
use App\Models\Album;
use App\Models\Role;
use Illuminate\Http\Request;

class SongController extends Controller
{
    public function index()
    {
        $songs = Song::all();
        return view('song.index', compact('songs'));
    }

    public function create()
    {
        $albums = Album::all();
        $role = Role::raw()->aggregate([
            [
                '$match' => ['name'=> 'artist']
            ],
            [
                '$project' => ['_id' => 1]
            ]
        ]);
        foreach ($role as $document) {
            $role = $document->_id;
        }
        $users = Role::find($role)->users()->get();
        
        return view ('song.create', compact(['albums', 'users']));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'string|required',
            'song' => 'file|required'
        ]);
        
        $album = Album::where('_id', $request->album)->first();
        
        $albumName = explode(' ', trim($album->name));
        $song = $request->file('song');
        $songExt = $song->getClientOriginalExtension();
        $songName = $request->name . "." .$songExt;
        $path = 'storage'.DIRECTORY_SEPARATOR.'albums'.DIRECTORY_SEPARATOR. $albumName[0];

        $song->move($path, $songName);
    
        $m_song = Song::create([
            'name' => $request->name,
            'url' => $songName
        ]);

        $album->songs()->attach($m_song);

        return redirect()->route('song.index')->with('success', 'A new song has been added successfully');
    }

    public function edit($id)
    {
        $song = Song::find($id);
        $albums = Album::all();
        $users = Role::find("60a7eff801660000250027a9")->users()->get();
        return view('song.edit', compact(['albums', 'song', 'users']));
    }

    public function destroy($id)
    {
        $song = Song::find($id);
        $song->delete();

        return redirect()->route('song.index')->with('success', ' Song was deleted successfully with ID: '.$id);
    }

    public function update(Request $request, $id)
    {   
        $song = Song::find($id);
        $song->update($request->only('name', 'artist', 'album', 'song'));
        return redirect()->route('song.index')->with('success', ' Song was updated successfully with ID: '.$id);
    }

}
