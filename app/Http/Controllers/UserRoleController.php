<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }

    public function create()
    {
        $role = Role::all();
        return view('user.create', compact('role'));
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->first_name = $request->input('fname');
        $user->last_name = $request->input('lname');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->username = $request->input('username');
        $user->save();

        $role = Role::find($request->input('role'));

        $user->roles()->attach($request->input('role'));
        $role->users()->attach($user);

        return redirect()->route('user.index')->with('success', ' User was added');
    }

    public function show($id)
    {
        $users = User::find($id);
        return view('user.show', compact('users'));
    }

    public function edit($id)
    {
        $users = User::find($id);
        $role = Role::all();
        return view('user.edit', compact(['users', 'role']));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->only('fname', 'lname', 'email', 'role'));
        $user->roles()->attach($request->only('role'));

        return redirect()->route('user.index')->with('success', ' User was updated successfully with ID: '.$user->id);
    }

    public function destroy($id)
    {
        $role = new Role();
        $user = User::find($id);
        $user->roles()->detach($role->id);
        $role->users()->detach($user->id);
        $user->delete();

        return redirect()->route('user.index')->with('success', ' User was deleted successfully with ID: '.$user->id);
    }
    
    public function destroyRole(Request $request, $id)
    {
        $role = new Role;
        $users = User::find($request->input('userid'));
        $users->roles()->detach($id);

        return redirect()->route('user.show', $users->id)->with('success', ' Role of user was deleted. User id: '.$users->id);
    }
}
