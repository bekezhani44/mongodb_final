<?php

namespace Database\Factories;

use App\Models\Album;
use App\Models\Genre;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AlbumFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Album::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=> $this->faker->sentence(2),
            'user_id'=>User::where('roles_ids', Role::where('name', 'artist')->pluck('_id')->first())->pluck('_id')->random(),
            'year'=> $this->faker->year,
            'genre_id' => Genre::pluck('_id')->random(),
            'cover'=> 'https://placeimg.com/265/265/any?' . rand(1, 100)
        ];
    }
}
