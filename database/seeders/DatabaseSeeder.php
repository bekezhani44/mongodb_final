<?php

namespace Database\Seeders;

use App\Models\Album;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(5)->create();

        $role1 = \App\Models\Role::create(['name' => 'user']);
        $role2 = \App\Models\Role::create(['name' => 'admin']);
        $role3 = \App\Models\Role::create(['name' => 'artist']);

        $roles = \App\Models\Role::all();
        $users = \App\Models\User::all();
        \App\Models\User::all()->each(function ($user) use ($roles) {
            $user->roles()->attach(
                $roles->random(rand(1, 3))->pluck('id')->toArray()
            );
        });

        \App\Models\Role::all()->each(function ($role) use ($users) {
            $role->users()->attach(
                $users->random(rand(1, 5))->pluck('id')->toArray()
            );
        });

        $admin = \App\Models\User::create(['email' => 'admin@admin.com', 'username'=>'admin', 'password'=>bcrypt('93Vesite'), 'remember_token' => Str::random(10)]);
        $admin->roles()->attach($role2->id);
        $role2->users()->attach($admin->id);

        \App\Models\Genre::factory(5)->create();
        \App\Models\Album::factory(10)->create();
        \App\Models\Tag::factory(5)->create();

        $tags = \App\Models\Tag::all();

        \App\Models\Album::all()->each(function ($album) use ($tags) {
            $album->tags()->attach(
                $tags->random(rand(1, 3))->pluck('id')->toArray()
            );
        });

        $albums = Album::all();
        \App\Models\Tag::all()->each(function ($tag) use ($albums) {
            $tag->albums()->attach(
                $albums->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }
}
