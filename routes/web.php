<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AlbumController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SongController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\UserRoleController;

use App\Models\Album;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home page
Route::redirect('/', '/home');
Route::get('/home', [HomeController::class, 'index'])->name('home.index');
Route::get('/albums', [HomeController::class, 'albums'])->name('home.albums');
Route::get('/admin', [HomeController::class, 'admin'])->middleware(['auth', 'auth.admin'])->name('home.admin');

// Album page
Route::get('/album', [AlbumController::class, 'index'])->name('album.index');
Route::get('/album/create', [AlbumController::class, 'create'])->name('album.create');
Route::get('/album/{album}', [AlbumController::class, 'show'])->name('album.show');
Route::get('/album/listen/{album}', [AlbumController::class, 'listen'])->name('album.listen');
Route::get('/album/like/{album_id}', [AlbumController::class, 'album_like'])->name('album.like');
Route::get('album/{album_id}/delete', [AlbumController::class, 'album_delete'])->name('album.delete');
Route::get('/album/gen/{genre}', [AlbumController::class, 'album_genre'])->name('album.genre');
Route::post('/album', [AlbumController::class, 'store'])->name('album.store');
Route::patch('/album/{album}', [AlbumController::class, 'update'])->name('album.update');
Route::delete('/album/{album}', [AlbumController::class, 'destroy'])->name('album.destroy');
Route::get('/album/{album}/edit', [AlbumController::class, 'edit'])->name('album.edit');

// Authorization page
Route::get('/signin', [AuthController::class, 'signin'])->name('auth.signin');
Route::post('/signin', [AuthController::class, 'post_signin'])->name('auth.post_signin');
Route::get('/signup', [AuthController::class, 'signup'])->name('auth.signup');
Route::post('/signup', [AuthController::class, 'post_signup'])->name('auth.post_signup');
Route::get('/logout', [AuthController::class, 'log_out'])->name('auth.log_out');

// User's roles
Route::get('/user', [UserRoleController::class, 'index'])->name('user.index');
Route::post('/user', [UserRoleController::class, 'store'])->name('user.store');
Route::get('/user/create', [UserRoleController::class, 'create'])->name('user.create');
// Route::patch('/user/{user}', [UserRoleController::class, 'update'])->name('user.update');
Route::delete('/user/{user}', [UserRoleController::class, 'destroy'])->name('user.destroy');
// Route::get('/user/{users}/edit', [UserRoleController::class, 'edit'])->name('user.edit');
Route::get('/user/{user}', [UserRoleController::class, 'show'])->name('user.show');
Route::delete('/user/{user}/destroyrole', [UserRoleController::class, 'destroyRole'])->name('user.destroyrole');

// Profile page
Route::get('user/{user}/profile', [ProfileController::class, 'index'])->name('profile.index');
Route::get('/user/{user}/station', [ProfileController::class, 'station'])->name('profile.station');
Route::get('/user/{user}/albums', [ProfileController::class, 'albums'])->name('profile.albums');
Route::get('/user/{user}/edit', [ProfileController::class, 'edit'])->name('profile.edit');
Route::patch('/user/{user}/', [ProfileController::class, 'update'])->name('profile.update');
Route::get('/user/album/{album_id}/delete', [ProfileController::class, 'delete_album'])->name('profile.album_delete');
Route::get('/user/{user}/album/create', [ProfileController::class, 'album_create'])->name('profile.album_create');
Route::get('/user/{user}/album/{album}/edit', [ProfileController::class, 'album_edit'])->name('profile.album_edit');
Route::get('user/{user}/song/create', [ProfileController::class, 'song_create'])->name('profile.song_create');
Route::get('user/{user}/songs', [ProfileController::class, 'songs'])->name('profile.songs');

// Search
Route::get('/search', [SearchController::class, 'index'])->middleware('auth')->name('search.index');
Route::get('/search/{id}', function ($id)  {
    switch ($id) {
        case 'asc' :
            return view('search.index', [
                'result' => Album::orderBy('name', 'asc')->get(),
                'sort' => "desc"
            ]);
        case 'desc' :
            return view('search.index', [
                'result' => Album::orderBy('name', 'desc')->get(),
                'sort' => "asc"
            ]);
    }
});

Route::get('/searchyear/{id}', function ($id)  {
    switch ($id) {
        case 'asc' :
            return view('search.index', [
                'result' => Album::orderBy('year', 'asc')->get(),
                'sort' => "desc"
            ]);
        case 'desc' :
            return view('search.index', [
                'result' => Album::orderBy('year', 'desc')->get(),
                'sort' => "asc"
            ]);
    }
});

// Song page
Route::get('/song', [SongController::class, 'index'])->name('song.index');
Route::post('/song', [SongController::class, 'store'])->name('song.store');
Route::get('/song/create', [SongController::class, 'create'])->name('song.create');
Route::patch('/song/{song}', [SongController::class, 'update'])->name('song.update');
Route::delete('/song/{song}', [SongController::class, 'destroy'])->name('song.destroy');
Route::get('/song/{song}/edit', [SongController::class, 'edit'])->name('song.edit');

// Genre 
Route::get('/genre', [GenreController::class, 'index'])->name('genre.index');
Route::post('/genre', [GenreController::class, 'store'])->name('genre.store');
Route::get('/genre/create', [GenreController::class, 'create'])->name('genre.create');
Route::patch('/genre/{genre}', [GenreController::class, 'update'])->name('genre.update');
Route::delete('/genre/{genre}', [GenreController::class, 'destroy'])->name('genre.destroy');
Route::get('/genre/{genre}/edit', [GenreController::class, 'edit'])->name('genre.edit');



